package edu.tcs.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.tcs.repository.AdminRepository;
import edu.tcs.repository.StudentRepository;


@Controller
public class HomeController extends HttpServlet  {


	private static final long serialVersionUID = -5934953255758442829L;
	
	
	private AdminRepository adminRepository;
	private StudentRepository studentRepository;

	@RequestMapping(value = "/Home", method = RequestMethod.GET)
	public String getHomeView() {
		return "Home";
	}
	@RequestMapping(value = "/loginadmin", method = RequestMethod.GET)
	public String getLoginAdminView() {
		return "Home_loginadmin";
	}
	
	@RequestMapping(value = "/log_admin")
	private void loginAdmin(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		String user_admin = request.getParameter("user");
		String password = request.getParameter("password");
		adminRepository = new AdminRepository();
		
		boolean test = adminRepository.loginAdmin(user_admin, password);
		if (test) {
	
			RequestDispatcher dispatcher = request.getRequestDispatcher("/admin/IndexA");
			dispatcher.forward(request, response);
		} else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/loginadmin");
			dispatcher.forward(request, response);
		}
	}
	
	@RequestMapping(value = "/loginuser", method = RequestMethod.GET)
	public String getLoginUerView() {
		
		return "Home_loginuser";
	}
	
	@RequestMapping(value = "/log_user")
	private void loginUser(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		String student_user = request.getParameter("user");
		String password = request.getParameter("password");


		studentRepository= new StudentRepository();
		boolean test = studentRepository.loginStudent(student_user, password);
		if (test) {
			System.out.println("Login");
			RequestDispatcher dispatcher = request.getRequestDispatcher("student/IndexS");
			dispatcher.forward(request, response);
		} else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/loginuser");
			dispatcher.forward(request, response);
		}
		
	}
}
