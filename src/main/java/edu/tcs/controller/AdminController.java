package edu.tcs.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.tcs.model.Book;
import edu.tcs.repository.BookRepository;



@Controller
@RequestMapping("/admin")
public class AdminController extends HttpServlet{
	
	
	private static final long serialVersionUID = 7633645973421254196L;
	private BookRepository bookRepository = new BookRepository();;
	

	
	@RequestMapping(value = "/IndexA")
	public String getAdminIndexView() {
		return "AdminHome";
	}
	@RequestMapping(value = "/AddBook")
	public String getAddBookView() {
		return "AdminAddBook";
	}
	@RequestMapping(value = "/SearchBook")
	public String getSearchBookView() {
		return "AdminSearchBook";
	}
	@RequestMapping(value = "/BookList")
	public String getBookListView() {
		return "AdminBookList";
	}
	@RequestMapping(value = "/DeleteBook")
	public String getDeleteBookView() {
		return "AdminDeleteBook";
	}
	@RequestMapping(value = "/UpdateBook")
	public String getUpdateBookView() {
		return "AdminUpdateBook";
	}
	@RequestMapping(value = "/AdminSucess")
	public String getSucessAdminView() {
		return "AdminSucess";
	}
	
	@RequestMapping(value = "/Book_list")
	private void listBook(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		List<Book> listBooks = bookRepository.getAllBooks();
		request.setAttribute("listBooks", listBooks);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/admin/BookList");
		dispatcher.forward(request, response);
	}
	
	@RequestMapping(value = "/Book_new_form")
	private void showNewForm(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/admin/AddBook");
		dispatcher.forward(request, response);
	}
	@RequestMapping(value = "/Book_edit")
	private void showEditForm(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, ServletException, IOException {
		Long id = Long.parseLong(request.getParameter("id"));
		Book existingBook = bookRepository.getBook(id);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/admin/AddBook");
		request.setAttribute("book", existingBook);
		dispatcher.forward(request, response);

	}
	
	@RequestMapping(value = "/Book_update")
	private void updateBook(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		Long id = Long.parseLong(request.getParameter("id"));
		String book_name = request.getParameter("bookName");
		String author = request.getParameter("author");
		String category = request.getParameter("category");
		int iSBN = Integer.parseInt(request.getParameter("ISBN"));
		int quantity = Integer.parseInt(request.getParameter("quantity"));

		Book book = new Book(book_name, author, iSBN, category, quantity);
		bookRepository.updateBook(book);
		response.sendRedirect("/admin/Book_list");
	}
	
	@RequestMapping(value = "/Book_delete")
	private void deleteBook(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		Long id = Long.parseLong(request.getParameter("id"));
		bookRepository.deleteBook(id);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/admin/AdminSucess");
		dispatcher.forward(request, response);
	}
}
