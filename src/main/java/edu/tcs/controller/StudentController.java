package edu.tcs.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.tcs.model.IssueBook;
import edu.tcs.repository.IssueBookRepository;


@Controller
@RequestMapping("/student")
public class StudentController extends HttpServlet {
	


	private static final long serialVersionUID = 6704576599341874313L;
	
	private IssueBookRepository issueBookRepository;

	@RequestMapping(value = "/IndexS")
	public String getStudentHomeView() {
		return "StudentHome";
	}
	
	@RequestMapping(value = "/Sucess")
	public String getSucessView() {
		return "StudentSuccess";
	}
	
	@RequestMapping(value = "/IssueBook")
	public String getIssueBookView() {
		return "StudentIssueBook";
	}
	
	@RequestMapping(value = "/Student_issueBook")
	private void issueBook(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException, ParseException {
		Long library_code = Long.parseLong(request.getParameter("library_code"));
		Long student_code = Long.parseLong(request.getParameter("student_code"));
		String issue_Date = request.getParameter("issue_date");
		String return_Date = request.getParameter("return_date");
	
		//Setting string into date format

		//Creating the object
		IssueBook issueBook = new IssueBook();
		issueBook.setLibrary_code(library_code);
		issueBook.setStudent_id(student_code);
		issueBook.setIssue_date(issue_Date);
		issueBook.setReturn_date(return_Date);
		
		issueBookRepository = new IssueBookRepository();
		issueBookRepository.issueABook(issueBook);
		

			RequestDispatcher dispatcher = request.getRequestDispatcher("StudentSucess");
			dispatcher.forward(request, response);
	
	}
	
}
