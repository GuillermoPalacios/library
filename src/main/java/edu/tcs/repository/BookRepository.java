package edu.tcs.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import javax.persistence.EntityTransaction;
import javax.persistence.Query;


import edu.tcs.model.Book;

import edu.tcs.utilities.JPA_Util;

public class BookRepository {
	

	
	
	//Working properly
	public void addBook(Book book) {
		EntityManager entity = JPA_Util.getEntityManagerFactory().createEntityManager();
        EntityTransaction transaction = entity.getTransaction();

        transaction.begin();
        //Here the date is in the format that I want to use
          
        entity.persist(book);
 
        //For createNativeQuery i have to used the name of my table
        transaction.commit();
          entity.close();
          
	}
	
	public Book getBook(Long id) {
		EntityManager entity = JPA_Util.getEntityManagerFactory().createEntityManager();
		Book book = new Book();
		book = entity.find(Book.class, id);
		return book;
	}
	
	public void updateBook(Book book) {
		EntityManager entity = JPA_Util.getEntityManagerFactory().createEntityManager();
		entity.getTransaction().begin();
		entity.merge(book);
		entity.getTransaction().commit();
		entity.close();
	}
	//Working properly
	public void deleteBook(Long id) {
		EntityManager entity = JPA_Util.getEntityManagerFactory().createEntityManager();
		Book book = new Book();
		book = entity.find(Book.class, id);
		entity.getTransaction().begin();
		entity.remove(book);
		entity.getTransaction().commit();
		entity.close();
		
	}
	
	
	public List<Book> getAllBooks() {
		EntityManager entity = JPA_Util.getEntityManagerFactory().createEntityManager();
		List<Book> book_list = new ArrayList<Book>();
		
		Query query = entity.createQuery("FROM book_info");
		book_list=query.getResultList();
		
		entity.close();
		
		return book_list;
		
	}
	
}
