package edu.tcs.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import edu.tcs.model.Admin;
import edu.tcs.model.Student;
import edu.tcs.utilities.JPA_Util;

public class StudentRepository {
	
	EntityManager entity = JPA_Util.getEntityManagerFactory().createEntityManager();
	
	
	//Working properly
	public void addStudent(Student student) {
		
        EntityTransaction transaction = entity.getTransaction();

        transaction.begin();
        //Here the date is in the format that I want to use
          
        entity.persist(student);
 
        //For createNativeQuery i have to used the name of my table
        transaction.commit();
          entity.close();
          
	}
	
	public Student getStudent(Long id) {
		Student student = new Student();
		student = entity.find(Student.class, id);
		return student;
	}
	
	public void updateStudent(Student student) {

		entity.getTransaction().begin();
		entity.merge(student);
		entity.getTransaction().commit();
		entity.close();
	}
	//Working properly
	public void deleteStudent(Long id) {
		Student student = new Student();
		student = entity.find(Student.class, id);
		entity.getTransaction().begin();
		entity.remove(student);
		entity.getTransaction().commit();
		
	}
	
	
	public List<Student> getAllBooks() {

		List<Student> student_list = new ArrayList<Student>();
		
		Query query = entity.createQuery("FROM book_info");
		student_list=query.getResultList();
		
		return student_list;
		

	}
	
	public boolean loginStudent(String user, String password)
	  {
	       try{
	    	   EntityTransaction transaction = entity.getTransaction();
	    	   transaction.begin();

       
	    	TypedQuery<Student> query = entity.createQuery("SELECT s FROM Student s WHERE s.name = :user AND s.password = :password", Student.class);
	        query.setParameter("user", user);
	        query.setParameter("password", password); 
	        try{ 
	        	Student s = query.getSingleResult();
	            return true;
	        }catch(javax.persistence.NoResultException e)
	        {
	            return false;
	        }
	        }
	        finally{
	        entity.close();
	        }

	    }
	
}
