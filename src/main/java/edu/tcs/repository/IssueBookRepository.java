package edu.tcs.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import javax.persistence.EntityTransaction;
import javax.persistence.Query;


import edu.tcs.model.IssueBook;
import edu.tcs.utilities.JPA_Util;

public class IssueBookRepository {
	
	EntityManager entity = JPA_Util.getEntityManagerFactory().createEntityManager();
	
	
	//Working properly
	public void issueABook(IssueBook issue) {
		
        EntityTransaction transaction = entity.getTransaction();

        transaction.begin();
        //Here the date is in the format that I want to use
          
        entity.persist(issue);
 
        //For createNativeQuery i have to used the name of my table
        transaction.commit();
          entity.close();
          
	}
	
	public IssueBook getIssue(Long id) {
		IssueBook issue = new IssueBook();
		issue = entity.find(IssueBook.class, id);
		return issue;
	}
	
	public void updateIssueStatus(IssueBook issue) {

		entity.getTransaction().begin();
		entity.merge(issue);
		entity.getTransaction().commit();
		entity.close();
	}
	//Working properly
	public void deleteIssue(Long id) {
		IssueBook issue_book = new IssueBook();
		issue_book = entity.find(IssueBook.class, id);
		entity.getTransaction().begin();
		entity.remove(issue_book);
		entity.getTransaction().commit();
		
	}
	
	
	public List<IssueBook> getAllIssues() {

		List<IssueBook> issue_book = new ArrayList<IssueBook>();
		
		Query query = entity.createQuery("FROM IssueBook");
		issue_book=query.getResultList();
		
		return issue_book;
		

	}
	
}
