package edu.tcs.testController;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


//I create this classjust in order to practice the scan packages from my ControllerServlet
@Controller
@RequestMapping
public class CoffeController {
	
	
	@RequestMapping(value = "/Welcome")
	public String shoWelcomePage(Model model) {
		
		//Sending data to view(to jsp)
		String name ="Guillermo ";
		model.addAttribute("myName", name);
		return "Welcome";
		
	}
	
	
	
}
