package edu.tcs.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity(name="Student")
@Table(name="Student")
public class Student implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "student_id")
	private Long   student_id;
	@NotBlank
	@Column(name = "student_name", length = 50)
	private String name;
	
	
	@Size(min = 10, max = 50, message 
		      = "About Me must be between 10 and 50 characters")
	@Column(name = "student_password", length = 50)
	private String password;
	@Email(message = "Email should be valid")
	@Column(name = "email", length = 50)
	private String email;
	
	
	public Student() {
		super();
	}
	public Student(String name, String password, String email) {
		super();
		this.name = name;
		this.password = password;
		this.email =  email;
	}
	public Long getStudent_id() {
		return student_id;
	}
	public void setStudent_id(Long student_id) {
		this.student_id = student_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	

	
	
	



}
