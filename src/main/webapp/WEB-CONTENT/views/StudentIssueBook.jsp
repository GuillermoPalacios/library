	<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
          <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
	<link href="<c:url value="/resources/theme/css/styleindex.css" />" rel="stylesheet">

    <title>Issue book</title>
  </head>

  <body>
    <!-- ***********************  HEADER / NAVIGATION *********************** -->
 <header class="header">
      <div class="container logo-nav-container">
        <a href="#" class="logo">LOGO</a>
        <!-- This span is in order to deploy the menu nav bar when the screen is small -->
        <span class="menu-icon">Menu</span>
        <nav class="">
          <ul class="navigation">
            <li style="color: blue;"><a href="IndexS" data-section="Home">Home</a></li>
            <li><a href="#">Actions</a>
              <ul class="sub-nav"> 
                <li><a href="IssueBook" data-section="IssueBook">Issue a book</a></li>
                <li><a href="#">Other Action</a></li>
              </ul>  
            <li><a href="#">Change password</a>
            <li><a href="#">Log out</a></li>
           
          </ul>
        </nav>
      </div>
    </header>
    <!-- ***********************  BODY *********************** -->
  <main class="main">
      <section class="Login_banner"><h1>Issue book</h1></section>    
            
  <section class="admin-form">
    <div class="Login">
      <h1>Issue book</h1>
    </div>
    <form
    class="main-form"
    action="Student_issueBook"
    autocomplete="on"
  >
  <label for="library_code"> Book code </label>
  <input
    class="form-item"
    id="library_code"
    type="text"
    name="library_code"
    placeholder="Book code here"
    required
  />
  
   <label for="student_code"> Student Code </label>
  <input
    class="form-item"
    id="student_code"
    type="text"
    name="student_code"
    placeholder="Your code here"
    required
  />
  <label for="issue_date"> Issue date </label>
  <input
    class="date"
    type="date"
    name="issue_date"
     id="issue_date"

  />
  <br>
    <label for="return_date"> Return date </label>
  <input
    class="date"
    type="date"
    name="return_date"
     id="retun_date"

  />
  

  <input
    class="button"
    type="submit"
    name="register"
 
    id="submit-button"
    value="Issue this book"
  />
</form>
 

  </section>
      
    </main>
   <!-- ***********************  FOOTER *********************** -->
    <footer class="footer">
      <div class="contact-info">
        <p>
          <strong>Contact info</strong>
          <br />
          Place
          <br />
          Queretaro
          <br />
          Other places
          <br />
          || CDMX || Guadalajara
          <br />
          Phone
          <br />
          2461103631
        </p>
      </div>
      <div class="copyright-container">
        <ul class="copyright-list">
          <li>© 2020 Test page for learning</li>
          <li><a href="IndexS" data-section="Home">|| Index</a></li>
          <li>
            <a href="IssueBook" data-section="IssueBook">|| IssueBook</a>
          </li>
        </ul>
      </div>
    </footer>
  </body>
</html>