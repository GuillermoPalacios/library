	<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
         <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!--  -->
	<link href="<c:url value="/resources/theme/css/style.css" />" rel="stylesheet">
<title>Form for adding a new book</title>
</head>
<body>
  <!-- ***********************  HEADER / NAVIGATION *********************** -->
 <header class="header">
      <div class="container logo-nav-container">
        <a href="#" class="logo">LOGO</a>
        <!-- This span is in order to deploy the menu nav bar when the screen is small -->
        <span class="menu-icon">Menu</span>
        <nav class="">
          <ul class="navigation">
            <li><a href="IndexA" data-section="Home">Index</a></li>
            <li><a href="#">About</a></li>
            <li><a href="#">Services</a>
            <ul class="sub-nav"> 
              <li><a href="AddBook" data-section="Add Book">Add Book</a></li>
              <li><a href="SearchBook">Search a Book</a></li>
              <li><a href="BookList">Display Books</a></li>
              <li><a href="DeleteBook">Delete Book</a></li>
              <li><a href="UpdateBook">Update Book</a></li>
            </ul>
          </li>
            <li><a href="#">Contact</a></li>
           
          </ul>
        </nav>
      </div>
    </header>
        <!-- ***********************  BODY *********************** -->
 <section class="register-form">
        <h1>Book registration</h1>
<form
          class="main-form"
          action="<%=request.getContextPath() %>/Book_register"
          method="post"
          autocomplete="on"
        >
          <label for="bookName"> Name </label>
          <input
            class="form-item"
            id="bookName"
            type="text"
            name="bookName"
            placeholder="Book name"
            required
          />
          <label for="author"> Author </label>
          <input
            class="form-item"
            id="author"
            type="text"
            name="author"
            placeholder="Author"
          />
            <label for="category"> Category </label>
          <input
            class="form-item"
            id="category"
            type="text"
            name="category"
            placeholder="Category"
          />
          
    <label for="ISBN"> ISBN </label>
          <input
            class="form-item"
            id="ISBN"
            type="text"
            name="ISBN"
            placeholder="ISBN"
          />
      
          <label for="quantity">Quantity </label>
          <input
            class="form-item"
            id="quantity"
            type="text"
            name="quantity"
            placeholder="Quantity"
          />

          <input
            class="button"
            type="submit"
            name="register"
          	 value="Add this book"
            id="submit-button"
          />
        </form>
           </section>
            <!-- ***********************  FOOTER *********************** -->
    <footer class="footer">
      <div class="contact-info">
        <p>
          <strong>Contact info</strong>
          <br />
          Place
          <br />
          Queretaro
          <br />
          Other places
          <br />
          ||CDMX || Guadalajara
          <br />
          Phone
          <br />
          2461103631
        </p>
      </div>
      <div class="copyright-container">
        <ul class="copyright-list">
          <li>© 2020 Test page for learning</li>
          <li><a href="IndexA" data-section="Home">|| Index</a></li>
          <li>
            <a href="AddBook" data-section="Add Book">|| Add Book</a>
          </li>
        </ul>
      </div>
    </footer>
</body>
</html>