	<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
       <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="<c:url value="/resources/theme/css/styleindex.css" />" rel="stylesheet">

    <title>Password reset</title>
  </head>

  <body>
    <!-- ***********************  HEADER / NAVIGATION *********************** -->
    <header class="header">
      <div class="container logo-nav-container">
        <a href="#" class="logo">LOGO</a>
        <!-- This span is in order to deploy the menu nav bar when the screen is small -->
        <span class="menu-icon">Menu</span>
        <nav class="">
          <ul class="navigation">
            <li style="color: blue;"><a href="Home" data-section="Home">Home</a></li>
            <li><a href="#">Login</a>
              <ul class="sub-nav"> 
                <li><a href="loginadmin" data-section="Form">Admin</a></li>
                <li><a href="loginuser">User</a></li>
              </ul>  
            <li><a href="#">Signup</a>
            <li><a href="#">Contact</a></li>
           
          </ul>
        </nav>
      </div>
    </header>
    <!-- ***********************  BODY *********************** -->

    <main class="main">
      <section class="Login_banner"><h1>New password</h1></section>    
            
  <section class="admin-form">
    <div class="Login">
      <h1>New Password</h1>
    </div>
    <form
    class="main-form"
    action="<%=request.getContextPath() %>/Login_newPassword"
    autocomplete="on"
  >
  <label for="user"> Insert your new password </label>
  <input
    class="form-item"
    id="password"
    type="text"
    name="password"
    placeholder="New password"
    required
  />
  
   <label for="user"> Confirm your new password </label>
  <input
    class="form-item"
    id="password2"
    type="text"
    name="password2"
    placeholder="Confirm password"
    required
  />

  <!-- intentar cambiar esto por un button que seria lo correcto -->
  <input
    class="button"
    type="submit"
    name="register"
    placeholder="SignIn"
    id="submit-button"
  />
</form>
 

  </section>
      
    </main>
    <!-- ***********************  FOOTER *********************** -->
    <footer class="footer">
      <div class="contact-info">
        <p>
          <strong>Contact info</strong>
          <br />
          Place
          <br />
          Queretaro
          <br />
          Other places
          <br />
          || CDMX || Guadalajara
          <br />
          Phone
          <br />
          2461103631
        </p>
      </div>
      <div class="copyright-container">
        <ul class="copyright-list">
          <li>© 2020 Test page for learning</li>
          <li><a href="Home.html" data-section="Home">|| Index</a></li>
          <li>
            <a href="form.html" data-section="Form">|| Form</a>
          </li>
        </ul>
      </div>
    </footer>
</html>
